## Run
./raytracer

## Binds
*1, 2, 3, 4* to change scene

*r* to render

## Samples Settings 

samples = 1 : 4 samples

samples = 2 : 16 samples

samples = 3 : 36 samples

samples = 4 : 64 samples

samples = 5 : 100 samples;
