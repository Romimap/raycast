//////////////////////////////////////////////////////////////////////////////
//
//  --- main.cpp ---
//  Created by Brian Summa
//
//  Edits by Romain Fournier
//
//////////////////////////////////////////////////////////////////////////////

#include "common.h"
#include "SourcePath.h"
#include "Object.h"
#include "Octree.h"

#include <thread>
#include <stdio.h>
#include <signal.h>
#include <stdio.h>
#include <signal.h>
#include <execinfo.h>
#include <stdlib.h>
#include <unistd.h>

using namespace Angel;

typedef vec4  color4;
typedef vec4  point4;


//Scene variables
enum{_SPHERE, _SQUARE, _BOX, _SCENE};
int scene = _SPHERE; //Simple sphere, square or cornell box
std::vector < Object * > sceneObjects;

point4 lightPosition = point4(0, 1.8, 1, 1);
color4 lightColor = color4(1, 1, 1, 1);
double lightForce = 3;
double lightSize = 0.8;

color4 environementColor = color4(.6, .8, 1, 1);
double environementForce = 1.1;

point4 cameraPosition;

//Recursion depth for raytracer
int maxDepth = 4;
int samples = 6;

namespace GLState {
  int window_width, window_height;

  bool render_line;

  std::vector < GLuint > objectVao;
  std::vector < GLuint > objectBuffer;

  GLuint vPosition, vNormal, vTexCoord;

  GLuint program;

  // Model-view and projection matrices uniform location
  GLuint  ModelView, ModelViewLight, NormalMatrix, Projection;

  //==========Trackball Variables==========
  static float curquat[4],lastquat[4];
  /* current transformation matrix */
  static float curmat[4][4];
  mat4 curmat_a;
  /* actual operation  */
  static int scaling;
  static int moving;
  static int panning;
  /* starting "moving" coordinates */
  static int beginx, beginy;
  /* ortho */
  float ortho_x, ortho_y;
  /* current scale factor */
  static float scalefactor;

  mat4  projection;
  mat4 sceneModelView;

  color4 light_ambient;
  color4 light_diffuse;
  color4 light_specular;

};

/* ------------------------------------------------------- */
/* -- PNG receptor class for use with pngdecode library -- */
class rayTraceReceptor : public cmps3120::png_receptor
{
private:
  const unsigned char *buffer;
  unsigned int width;
  unsigned int height;
  int channels;
  
public:
  rayTraceReceptor(const unsigned char *use_buffer,
                   unsigned int width,
                   unsigned int height,
                   int channels){
    this->buffer = use_buffer;
    this->width = width;
    this->height = height;
    this->channels = channels;
  }
  cmps3120::png_header get_header(){
    cmps3120::png_header header;
    header.width = width;
    header.height = height;
    header.bit_depth = 8;
    switch (channels)
    {
      case 1:
      header.color_type = cmps3120::PNG_GRAYSCALE;break;
      case 2:
      header.color_type = cmps3120::PNG_GRAYSCALE_ALPHA;break;
      case 3:
      header.color_type = cmps3120::PNG_RGB;break;
      default:
      header.color_type = cmps3120::PNG_RGBA;break;
    }
    return header;
  }
  cmps3120::png_pixel get_pixel(unsigned int x, unsigned int y, unsigned int level){
    cmps3120::png_pixel pixel;
    unsigned int idx = y*width+x;
    /* pngdecode wants 16-bit color values */
    pixel.r = buffer[4*idx]*257;
    pixel.g = buffer[4*idx+1]*257;
    pixel.b = buffer[4*idx+2]*257;
    pixel.a = buffer[4*idx+3]*257;
    return pixel;
  }
};

/* -------------------------------------------------------------------------- */
/* ----------------------  Write Image to Disk  ----------------------------- */
bool write_image(const char* filename, const unsigned char *Src,
                 int Width, int Height, int channels){
  cmps3120::png_encoder the_encoder;
  cmps3120::png_error result;
  rayTraceReceptor image(Src,Width,Height,channels);
  the_encoder.set_receptor(&image);
  result = the_encoder.write_file(filename);
  if (result == cmps3120::PNG_DONE)
    std::cerr << "finished writing "<<filename<<"."<<std::endl;
  else
    std::cerr << "write to "<<filename<<" returned error code "<<result<<"."<<std::endl;
  return result==cmps3120::PNG_DONE;
}


/* -------------------------------------------------------------------------- */
/* -------- Given OpenGL matrices find ray in world coordinates of ---------- */
/* -------- window position x,y --------------------------------------------- */
std::vector < vec4 > findRay(GLdouble x, GLdouble y){
  
  y = GLState::window_height-y;
  
  int viewport[4];
  glGetIntegerv(GL_VIEWPORT, viewport);
  
  GLdouble modelViewMatrix[16];
  GLdouble projectionMatrix[16];
  for(unsigned int i=0; i < 4; i++){
    for(unsigned int j=0; j < 4; j++){
      modelViewMatrix[j*4+i]  =  GLState::sceneModelView[i][j];
      projectionMatrix[j*4+i] =  GLState::projection[i][j];
    }
  }
  
  
  GLdouble nearPlaneLocation[3];
  _gluUnProject(x, y, 0.0, modelViewMatrix, projectionMatrix,
                viewport, &nearPlaneLocation[0], &nearPlaneLocation[1],
                &nearPlaneLocation[2]);
  
  GLdouble farPlaneLocation[3];
  _gluUnProject(x, y, 1.0, modelViewMatrix, projectionMatrix,
                viewport, &farPlaneLocation[0], &farPlaneLocation[1],
                &farPlaneLocation[2]);
  
  
  vec4 ray_origin = vec4(nearPlaneLocation[0], nearPlaneLocation[1], nearPlaneLocation[2], 1.0);
  vec3 temp = vec3(farPlaneLocation[0]-nearPlaneLocation[0],
                   farPlaneLocation[1]-nearPlaneLocation[1],
                   farPlaneLocation[2]-nearPlaneLocation[2]);
  temp = normalize(temp);
  vec4 ray_dir = vec4(temp.x, temp.y, temp.z, 0.0);
  
  std::vector < vec4 > result(2);
  result[0] = ray_origin;
  result[1] = ray_dir;
  
  return result;
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
bool intersectionSort(Object::IntersectionValues i, Object::IntersectionValues j){
  return (i.t < j.t);
}

/* -------------------------------------------------------------------------- */
/* ---------  Some debugging code: cast Ray = p0 + t*dir  ------------------- */
/* ---------  and print out what it hits =                ------------------- */
void castRayDebug(vec4 p0, vec4 dir){
  
  std::vector < Object::IntersectionValues > intersections;

  vec3 p = vec3(p0.x, p0.y, p0.z);
  vec3 v = vec3(dir.x, dir.y, dir.z);
  
  for(unsigned int i=0; i < sceneObjects.size(); i++){
    intersections.push_back(sceneObjects[i]->intersect(p, v));
    intersections[intersections.size()-1].ID_ = i;
  }
  
  for(unsigned int i=0; i < intersections.size(); i++){
    if(intersections[i].t != std::numeric_limits< double >::infinity()){
      std::cout << "Hit " << intersections[i].name << " " << intersections[i].ID_ << "\n";
      std::cout << "P: " <<  intersections[i].P << "\n";
      std::cout << "N: " <<  intersections[i].N << "\n";
      vec4 L = lightPosition-intersections[i].P;
      L  = normalize(L);
      std::cout << "L: " << L << "\n";
    }
  }
  
}

double mix(double x, double y, double a) {
  return x*(1-a)+y*a;
}

vec4 mix(vec4 x, vec4 y, double a) {
  return x*(1-a)+y*a;
}

double map(double s, double a1, double a2, double b1, double b2) {
  return b1 + (s-a1)*(b2-b1)/(a2-a1);
}

/* Utilisé par les limières directes spéculaires 
*
*/
double roughnessProfile(double a, double r) {
  r = 1 - r;
  //Profile
  if (r < 0.1) {
    r = map(r, 0, 0.1, 1, 2);
  } else if (r < 0.5) {
    r = map(r, 0.1, 0.5, 2, 8);
  } else if (r < 0.8) {
    r = map(r, 0.5, 0.8, 8, 64);
  } else if (r < 0.9) {
    r = map(r, 0.8, 0.9, 64, 512);
  } else {
    r = map(r, 0.9, 1, 512, 4096);
  }
  
  return pow((cos(a) + 1.) / 2., r);
}

Object::IntersectionValues intersect(vec3 p0, vec3 E) {
  Object::IntersectionValues intersectionValues;
  intersectionValues.t = INFINITY;
  Object* object = nullptr;

  for (Object* o : sceneObjects) {
    Object::IntersectionValues iv = o->intersect(p0, E);
    if (iv.t < intersectionValues.t) {
      intersectionValues = iv;
      object = o;
    }
  }

  return intersectionValues;
}

double dRand() {
  return (double)rand()/(double)RAND_MAX;
}

/* Tire un rayon d'ombre 
*
*/
double getLightCoverage(vec3 p, vec3 lp, vec3 n){
  vec3 d = lp - p;
  double t = length(d);

  double ga = 2.39996;
  vec3 x = normalize(cross(d, n));
  vec3 y = normalize(cross(d, x));

  double theta = dRand() * 2 * M_PI;
  double distance = dRand() * lightSize;
  vec3 pos = lp + ((x * cos(theta) * distance) + (y * sin(theta) * distance));
  vec3 dir = normalize(pos - p);
  for (Object* o : sceneObjects) {
    Object::IntersectionValues iv = o->intersect(p + EPSILON * 10 * dir, dir);
    if (iv.t < t) {
      return 0;
    }
  }

  return 1;
}

/* Calcule un rebonds diffus 
*
*/
vec4 getDiffuseBounce(vec4 E, vec4 N) {
  vec3 e = vec3(E.x, E.y, E.z);
  vec3 n = vec3(N.x, N.y, N.z);
  vec3 t = cross(e, n);

  double lat = dRand() * M_PI / 2;
  double lon = dRand() * M_PI * 2;

  vec3 b = vec3(n.x, n.y, n.z);
  b = rotate(b, t, lat);
  b = rotate(b, n, lon);
  b = normalize(b);

  return vec4(b, 1);
}

/* Calcule un rebonds spéculaire 
*
*/
vec4 getSpecularBounce(vec4 E, vec4 N, double roughness) {
  vec3 e = vec3(E.x, E.y, E.z);
  vec3 n = vec3(N.x, N.y, N.z);
  
  vec3 t = cross(e, n);
  n = normalize(n);
  vec3 reflection = e - 2.0 * dot(n, e) * n;

  double lat = dRand() * M_PI * pow(roughness, 2);
  double lon = dRand() * M_PI * 2.;

  vec3 b = vec3(reflection.x, reflection.y, reflection.z);
  b = rotate(b, t, lat);
  b = rotate(b, reflection, lon);
  b = normalize(b);

  return vec4(b, 1);
}

/* Calcule un rebonds "Réfraction" 
*
*/
vec4 getRefractionBounce(vec4 E, vec4 N, double roughness, double n1, double n2) {
  //n₁sin(θ₁) = n₂sin(θ₂)
  //asin(n₁sin(θ₁)/n₂) = θ₂
  vec3 e = vec3(E.x, E.y, E.z);
  vec3 n = vec3(N.x, N.y, N.z);
  n = normalize(n);

  vec3 refraction;
  vec3 inVec;
  double inAngle;
  if (angle(e, n) > M_PI/2) { // From Outside
    inVec = -e;
    refraction = vec3(-n);
    inAngle = angle(inVec, n);
  } else { // From Inside
    inVec = e;
    refraction = vec3(n);
    inAngle = angle(inVec, n);

  }

  vec3 t = cross(inVec, n);
  t = normalize(t);
  double refractionAngle = asin((n1 * sin(inAngle)) / n2); 
  refraction = rotate(refraction, -t, refractionAngle);

  double lat = dRand() * M_PI * pow(roughness, 2);
  double lon = dRand() * M_PI * 2.;

  vec3 b = vec3(refraction.x, refraction.y, refraction.z);
  b = rotate(b, t, lat);
  b = rotate(b, refraction, lon);
  b = normalize(b);

  return vec4(b, 1);
}

/* Retourne la force de la lumière en fonction de sa distance
   Mauvaise formule, mais ca marche mieux
*
*/
float getLightIntensity(vec3 position, vec3 lightPosition) {
  vec3 direction = lightPosition - position;
  float distance = length(direction);
  return 1 / (1 + distance) * lightForce;
}

/* Retourne l'intensité de la lumière directe diffuse 
*
*/
float getDiffuseIntensity(vec3 direction, vec3 normal, vec3 in) {
  normal = normalize(normal);
  vec3 reflection = in - 2.0 * dot(normal, in) * normal;

  double angleDiffuse = angle(direction, normal) * 2; //0 -> PI

  if (angleDiffuse > M_PI) angleDiffuse = M_PI;

  double diffuseValue = (cos(angleDiffuse) + 1.) / 2.;

  return diffuseValue;
}

/* Rentourne l'intensité de la lumière directe spéculaire 
*
*/
float getSpecularIntensity(vec3 direction, vec3 normal, vec3 in, double roughness) {
  normal = normalize(normal);
  vec3 reflection = in - 2.0 * dot(normal, in) * normal;

  double angleSpecular = angle(direction, reflection) * 2; //0 -> PI

  if (angleSpecular > M_PI) angleSpecular = M_PI;

  double specularValue = roughnessProfile(angleSpecular, roughness);

  return specularValue;
}

/* Retourne la force de l'effet fresnel 
*
*/
double getFresnel(vec3 direction, vec3 normal, double metallic) {
  vec3 d = -direction;
  double ratio = (std::min(angle(d, normal), M_PI / 2.1)) / (M_PI / 2.1);
  double fresnel = map(ratio, 0.5, 1, metallic, 1);
  return pow(fresnel, 3);
}


double max (double a, double b) {
  if (a > b) return a;
  return b;
}

/* -------------------------------------------------------------------------- */
/* ----------  cast Ray = p0 + t*dir and intersect with sphere      --------- */
/* ----------  return color, right now shading is approx based      --------- */
/* ----------  depth                                                --------- */
vec4 castRay(vec4 p0, vec4 E, Object *lastHitObject, int depth){
  vec4 color = vec4(0,0,0,1);
  vec3 p = vec3(p0.x, p0.y, p0.z);
  vec3 v = vec3(E.x, E.y, E.z);
  
  if(depth > maxDepth){ return color; }
  
  //Check for the closest intersection
  Object::IntersectionValues intersectionValues = intersect(p, v);
  Object* object = intersectionValues.o;

  //Intersection
  if (object != nullptr && intersectionValues.t < INFINITY) {
    vec3 cp = p + v * intersectionValues.t;                                                 //Collision point
    vec3 lp = vec3(lightPosition.x, lightPosition.y, lightPosition.z);                      //Light position
    vec3 lDir = normalize(lp - cp);                                                         //Light direction
    vec3 n = vec3(intersectionValues.N.x, intersectionValues.N.y, intersectionValues.N.z);  //Normal

    // ****** Out Refraction
    if (angle(v, n) < M_PI/2) { //We are on the inside of an object, we only compute the out refraction if there is one to compute
      if (dRand() < object->shadingValues.refraction) {
        // *** Refraction
        vec4 newERefraction = getRefractionBounce(E, intersectionValues.N, object->shadingValues.roughness, object->shadingValues.ior, 1);
        vec4 newPRefraction = vec4(cp, 1) + newERefraction * EPSILON;
        newPRefraction.w = 1;
        color += castRay(newPRefraction, newERefraction, object, depth + 1) * object->shadingValues.color;
        color.w = 1;
      }
      return color;
    }

    // ****** Specularity and Diffuse
    // Here we compute the specularity based on the metallic value of the object, and the fresnel effect
    double specular = map(object->shadingValues.metallic, 0, 1, 0.5, 1) * getFresnel(v, n, object->shadingValues.metallic);
    double diffuse = map(object->shadingValues.metallic, 0, 1, 1, 0);
    
    // ****** Debug
    //return object->shadingValues.color;                                                   //COLOR    
    //return vec4(n, 1);                                                                    //NORMALS
    //return vec4(intersectionValues.t, intersectionValues.t, intersectionValues.t, 1);     //DEPTH
    //return vec4(specular, specular, specular, 1);                                         //SPECULAR
    //return vec4(intersectionValues.u, intersectionValues.v, 0, 1);                        //UV triangle

    // ****** Direct light
    //Here we treat light as a "flow".
    //we have to process the distance and the angle at witch the light hit that point, 
    //the farthest we are from the source, and the shallowest the face is, the fainter the light will be
    vec4 lightBase = vec4(0, 0, 0, 1);
    lightBase += lightColor * getLightCoverage(cp, lp, n) * getLightIntensity(p, lp) * object->shadingValues.color;
    // *** Diffuse
    color += lightBase * getDiffuseIntensity(lDir, n, v) * (1 - specular) * (1 - object->shadingValues.refraction);
    // *** Specular
    color += lightBase * getSpecularIntensity(lDir, n, v, object->shadingValues.roughness) * specular; 



    vec4 specularColor = vec4(0);
    vec4 diffuseColor = vec4(0);
    vec4 refractionColor = vec4(0);

    // ****** Indirect light
    //Here we treat the light as "photons", 
    //the light will automatically be fainter from a distance, and from a shallow angle as those "photons" will tend to 
    //separate from each other, like a flow of light would
    if (dRand() < specular) { //Here, a proportion of rays will be diffuse rays, and specular rays based on the roughness 
      // *** Specular
      vec4 newESpecular = getSpecularBounce(E, intersectionValues.N, object->shadingValues.roughness);
      vec4 newPSpecular = vec4(cp, 1) + newESpecular * EPSILON;
      newPSpecular.w = 1;
      specularColor = castRay(newPSpecular, newESpecular, object, depth + 1);// * object->shadingValues.color;
    } 
    if (dRand() < diffuse) {
      if (dRand() < object->shadingValues.refraction) {
        // *** Refraction
        vec4 newERefraction = getRefractionBounce(E, intersectionValues.N, object->shadingValues.roughness, 1, object->shadingValues.ior);
        vec4 newPRefraction = vec4(cp, 1) + newERefraction * EPSILON;
        newPRefraction.w = 1;
        refractionColor = castRay(newPRefraction, newERefraction, object, depth + 1) * object->shadingValues.color;
      } else {
        // *** Diffuse
        vec4 newEDiffuse = getDiffuseBounce(E, intersectionValues.N);
        vec4 newPDiffuse = vec4(cp, 1) + newEDiffuse * EPSILON;
        newPDiffuse.w = 1;
        diffuseColor = castRay(newPDiffuse, newEDiffuse, object, depth + 1) * object->shadingValues.color;
      }
    }


    specularColor = clampPos(specularColor);
    diffuseColor = clampPos(diffuseColor);
    refractionColor = clampPos(refractionColor);

    color += specularColor + diffuseColor + refractionColor;

    // ****** Absorption
    color *= object->shadingValues.transmission;
  } else { //Environement
    color += environementColor * environementForce;
  }

  color.w = 1;
  return color;
}



/* -------------------------------------------------------------------------- */
/* ------------  Ray trace our scene.  Output color to image and    --------- */
/* -----------   Output color to image and save to disk             --------- */
void rayTrace(){
  
  unsigned char *buffer = new unsigned char[GLState::window_width*GLState::window_height*4];

  for(unsigned int i=0; i < GLState::window_width; i++){
    
    printf("%d%%                                                       \n", (int)(((float)i / 767.f)*100));
    printf("                                                      \r[");
    for (int b = 0; b < 60; b++) {
      if ((int)(((float)i / 767.f)*60) < b)
        printf("-");
      else
        printf("=");
    }
    printf("]\n\033[2A");
    for(unsigned int j=0; j < GLState::window_height; j++){
      
      int idx = j*GLState::window_width+i;
      vec4 color = vec4(0,0,0,0);
      for (int x = -samples; x < samples; x++) {
        for (int y = -samples; y < samples; y++) {
          float xx = (float)x / (float)(2*samples);
          float yy = (float)y / (float)(2*samples);
          std::vector < vec4 > ray_o_dir = findRay(i + xx,j + yy);
          color += castRay(ray_o_dir[0], vec4(ray_o_dir[1].x, ray_o_dir[1].y, ray_o_dir[1].z,0), NULL, 0);
        }
      }
      //AVG samples
      color /= (double)((samples * 2)*(samples * 2));
      //Color management
      color.x = std::max(color.x, 0.f);
      color.y = std::max(color.y, 0.f);
      color.z = std::max(color.z, 0.f);
      color.w = std::max(std::min(color.w, 1.f), 0.f);
      color.x = tanh(color.x); //From 0->+infinity to 0->1
      color.y = tanh(color.y);
      color.z = tanh(color.z);
      buffer[4*idx]   = color.x*255;
      buffer[4*idx+1] = color.y*255;
      buffer[4*idx+2] = color.z*255;
      buffer[4*idx+3] = color.w*255;
    }
  }

  printf("\rdone !\n\n");
  
  write_image("output.png", buffer, GLState::window_width, GLState::window_height, 4);
  
  delete[] buffer;
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
static void error_callback(int error, const char* description)
{
  fprintf(stderr, "Error: %s\n", description);
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void initCornellBox(){
  cameraPosition = point4( 0.0, 0.0, 6.0, 1.0 );
  
  sceneObjects.clear();
  
  { //Back Wall
    sceneObjects.push_back(new Square("Back Wall", Translate(0.0, 0.0, -2.0)*Scale(2.0,2.0,1.0)));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(1, 1,1.0,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    _shadingValues.metallic = 0;

    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
  { //Left Wall
    sceneObjects.push_back(new Square("Left Wall", RotateY(90)*Translate(0.0, 0.0, -2.0)*Scale(2.0,2.0,1.0)));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(1.0,.2,.2,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
  { //Right Wall
    sceneObjects.push_back(new Square("Right Wall", RotateY(-90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0 )));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.3,1,.3,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
  { //Floor
    sceneObjects.push_back(new Square("Floor", RotateX(-90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0)));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(1.0,1.0,1.0,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
  { //Ceiling
    sceneObjects.push_back(new Square("Ceiling", RotateX(90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0)));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(1.0,1.0,1.0,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }  

  { //Front Wall
    sceneObjects.push_back(new Square("Front Wall",RotateX(180)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0)));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.5, .5, .5,1);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    _shadingValues.roughness = 0.9;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
  
  /*  {
  sceneObjects.push_back(new Box("Box1", Scale(.6)*Translate(-0.8, -2.5, -1)*RotateY(60)));
  Object::ShadingValues _shadingValues;
  _shadingValues.color = vec4(1, .2, .2, 1);
  _shadingValues.Ka = 0.0;
  _shadingValues.Kd = 0.0;
  _shadingValues.Ks = 0.0;
  _shadingValues.Kn = 16.0;
  _shadingValues.Kt = 1.0;
  _shadingValues.Kr = 1.4;
  sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
  sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }/**/

 {
   sceneObjects.push_back(new Sphere("Sphere1", vec3(-0.5, -1.3, -0.5), .7));
   Object::ShadingValues _shadingValues;
   _shadingValues.color = vec4(.8,.8,.8,1.0);
   _shadingValues.Ka = 0.0;
   _shadingValues.Kd = 0.0;
   _shadingValues.Ks = 0.0;
   _shadingValues.Kn = 16.0;
   _shadingValues.Kt = 1.0;
   _shadingValues.Kr = 1.4;
   _shadingValues.roughness = 0;
   _shadingValues.metallic = 0;
   _shadingValues.refraction = 1;
   _shadingValues.ior = 1.5;
   sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
   sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }

  {
   sceneObjects.push_back(new Sphere("Sphere2", vec3(0.5, -1.3, 0.5), .7));
   Object::ShadingValues _shadingValues;
   _shadingValues.color = vec4(.15,.15,.15,1.0);
   _shadingValues.Ka = 0.0;
   _shadingValues.Kd = 0.0;
   _shadingValues.Ks = 0.0;
   _shadingValues.Kn = 16.0;
   _shadingValues.Kt = 1.0;
   _shadingValues.Kr = 1.4;
   _shadingValues.roughness = 0;
   _shadingValues.metallic = 1;
   _shadingValues.refraction = 0;
   _shadingValues.ior = 1.5;
   sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
   sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }

  // {
  // sceneObjects.push_back(new Box("Box2", Translate(0.5, -.8, -0.7)*RotateY(20)*Scale(.7, 1.2, .7)));
  // Object::ShadingValues _shadingValues;
  // _shadingValues.color = vec4(1.0,1.0,1.0,1.0);
  // _shadingValues.Ka = 0.0;
  // _shadingValues.Kd = 0.0;
  // _shadingValues.Ks = 0.0;
  // _shadingValues.Kn = 16.0;
  // _shadingValues.Kt = 1.0;
  // _shadingValues.Kr = 1.4;
  // sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
  // sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  // }
}


/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void initUnitSphere(){
  cameraPosition = point4( 0.0, 0.0, 3.0, 1.0 );
  
  sceneObjects.clear();
  
  {
  sceneObjects.push_back(new Sphere("Diffuse sphere"));
  Object::ShadingValues _shadingValues;
  _shadingValues.color = vec4(1.0,1.0,1.0,1.0);
  _shadingValues.Ka = 0.0;
  _shadingValues.Kd = 1.0;
  _shadingValues.Ks = 0.0;
  _shadingValues.Kn = 16.0;
  _shadingValues.Kt = 0.0;
  _shadingValues.Kr = 0.0;
  sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
  sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void initUnitSquare(){
  cameraPosition = point4( 0.0, 2.0, 6.0, 1.0 );

  sceneObjects.clear();

  { //Back Wall
    sceneObjects.push_back(new Square("Unit Square"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(1.0,1.0,1.0,1.0);
    _shadingValues.Ka = 0.0;
    _shadingValues.Kd = 1.0;
    _shadingValues.Ks = 0.0;
    _shadingValues.Kn = 16.0;
    _shadingValues.Kt = 0.0;
    _shadingValues.Kr = 0.0;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
}

void initScene() {
  cameraPosition = point4( 0.0, .3, 4.0, 1.0);
  sceneObjects.clear();
  lightPosition = point4 (20, 20, -50, 1);
  lightColor = vec4(1, .8, .7);
  lightForce = 100;

  { //Grass
    sceneObjects.push_back(new MeshObj("Grass", "./scene/grass.obj"));
    Object::ShadingValues _shadingValues;   
    _shadingValues.color = vec4(.5,.8,.7,1.0);
    _shadingValues.roughness = 1;
    _shadingValues.metallic = 0;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  { //Water
    sceneObjects.push_back(new MeshObj("Water", "./scene/water.obj"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.5,.6,.8,1.0);
    _shadingValues.roughness = 0.001;
    _shadingValues.metallic = 1;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  { //Log
    sceneObjects.push_back(new MeshObj("Log", "./scene/log.obj"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.8,.45,.25,1.0);
    _shadingValues.roughness = 0.9;
    _shadingValues.metallic = 0;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  { //Leafs
    sceneObjects.push_back(new MeshObj("Leafs", "./scene/leafs.obj"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.3,.8,.25,1.0);
    _shadingValues.roughness = 0.9;
    _shadingValues.metallic = 0;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  { //Deer
    sceneObjects.push_back(new MeshObj("Deer", "./scene/deer.obj"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.8,.3,.25,1.0);
    _shadingValues.roughness = 1;
    _shadingValues.metallic = 0;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
  { //Antlers
    sceneObjects.push_back(new MeshObj("Antlers", "./scene/antlers.obj"));
    Object::ShadingValues _shadingValues;
    _shadingValues.color = vec4(.25,.25,.25,1.0);
    _shadingValues.roughness = 0.1;
    _shadingValues.metallic = 0;
    _shadingValues.transmission = 1;
    _shadingValues.refraction = 0;
    _shadingValues.ior = 1;
    sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
    sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
  }
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);
  if (key == GLFW_KEY_1 && action == GLFW_PRESS){
    scene = _SPHERE;
    initUnitSphere();
  }
  if (key == GLFW_KEY_2 && action == GLFW_PRESS){
    scene = _SQUARE;
    initUnitSquare();
  }
  if (key == GLFW_KEY_3 && action == GLFW_PRESS){
    scene = _BOX;
    initCornellBox();
  }
  if (key == GLFW_KEY_4 && action == GLFW_PRESS){
    scene = _SCENE;
    initScene();
  }
  if (key == GLFW_KEY_R && action == GLFW_PRESS)
    rayTrace();
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
static void mouseClick(GLFWwindow* window, int button, int action, int mods){

  if (GLFW_RELEASE == action){
    GLState::moving=GLState::scaling=GLState::panning=false;
    return;
  }

  if( mods & GLFW_MOD_SHIFT){
    GLState::scaling=true;
  }else if( mods & GLFW_MOD_ALT ){
    GLState::panning=true;
  }else{
    GLState::moving=true;
    TrackBall::trackball(GLState::lastquat, 0, 0, 0, 0);
  }

  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);
  GLState::beginx = xpos; GLState::beginy = ypos;

  std::vector < vec4 > ray_o_dir = findRay(xpos, ypos);
  castRayDebug(ray_o_dir[0], vec4(ray_o_dir[1].x, ray_o_dir[1].y, ray_o_dir[1].z,0.0));

}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void mouseMove(GLFWwindow* window, double x, double y){

  int W, H;
  glfwGetFramebufferSize(window, &W, &H);


  float dx=(x-GLState::beginx)/(float)W;
  float dy=(GLState::beginy-y)/(float)H;

  if (GLState::panning)
    {
    GLState::ortho_x  +=dx;
    GLState::ortho_y  +=dy;

    GLState::beginx = x; GLState::beginy = y;
    return;
    }
  else if (GLState::scaling)
    {
    GLState::scalefactor *= (1.0f+dx);

    GLState::beginx = x;GLState::beginy = y;
    return;
    }
  else if (GLState::moving)
    {
    TrackBall::trackball(GLState::lastquat,
                         (2.0f * GLState::beginx - W) / W,
                         (H - 2.0f * GLState::beginy) / H,
                         (2.0f * x - W) / W,
                         (H - 2.0f * y) / H
                         );

    TrackBall::add_quats(GLState::lastquat, GLState::curquat, GLState::curquat);
    TrackBall::build_rotmatrix(GLState::curmat, GLState::curquat);

    GLState::beginx = x;GLState::beginy = y;
    return;
    }
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void initGL(){

  GLState::light_ambient  = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );
  GLState::light_diffuse  = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );
  GLState::light_specular = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );


  std::string vshader = source_path + "/shaders/vshader.glsl";
  std::string fshader = source_path + "/shaders/fshader.glsl";

  GLchar* vertex_shader_source = readShaderSource(vshader.c_str());
  GLchar* fragment_shader_source = readShaderSource(fshader.c_str());

  GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, (const GLchar**) &vertex_shader_source, NULL);
  glCompileShader(vertex_shader);
  check_shader_compilation(vshader, vertex_shader);

  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, (const GLchar**) &fragment_shader_source, NULL);
  glCompileShader(fragment_shader);
  check_shader_compilation(fshader, fragment_shader);

  GLState::program = glCreateProgram();
  glAttachShader(GLState::program, vertex_shader);
  glAttachShader(GLState::program, fragment_shader);

  glLinkProgram(GLState::program);
  check_program_link(GLState::program);

  glUseProgram(GLState::program);

  glBindFragDataLocation(GLState::program, 0, "fragColor");

  // set up vertex arrays
  GLState::vPosition = glGetAttribLocation( GLState::program, "vPosition" );
  GLState::vNormal = glGetAttribLocation( GLState::program, "vNormal" );

  // Retrieve transformation uniform variable locations
  GLState::ModelView = glGetUniformLocation( GLState::program, "ModelView" );
  GLState::NormalMatrix = glGetUniformLocation( GLState::program, "NormalMatrix" );
  GLState::ModelViewLight = glGetUniformLocation( GLState::program, "ModelViewLight" );
  GLState::Projection = glGetUniformLocation( GLState::program, "Projection" );

  GLState::objectVao.resize(sceneObjects.size());
  glGenVertexArrays( sceneObjects.size(), &GLState::objectVao[0] );

  GLState::objectBuffer.resize(sceneObjects.size());
  glGenBuffers( sceneObjects.size(), &GLState::objectBuffer[0] );

  for(unsigned int i=0; i < sceneObjects.size(); i++){
    glBindVertexArray( GLState::objectVao[i] );
    glBindBuffer( GL_ARRAY_BUFFER, GLState::objectBuffer[i] );
    size_t vertices_bytes = sceneObjects[i]->mesh.vertices.size()*sizeof(vec4);
    size_t normals_bytes  =sceneObjects[i]->mesh.normals.size()*sizeof(vec3);

    glBufferData( GL_ARRAY_BUFFER, vertices_bytes + normals_bytes, NULL, GL_STATIC_DRAW );
    size_t offset = 0;
    glBufferSubData( GL_ARRAY_BUFFER, offset, vertices_bytes, &sceneObjects[i]->mesh.vertices[0] );
    offset += vertices_bytes;
    glBufferSubData( GL_ARRAY_BUFFER, offset, normals_bytes,  &sceneObjects[i]->mesh.normals[0] );

    glEnableVertexAttribArray( GLState::vNormal );
    glEnableVertexAttribArray( GLState::vPosition );

    glVertexAttribPointer( GLState::vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    glVertexAttribPointer( GLState::vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertices_bytes));

  }



  glEnable( GL_DEPTH_TEST );
  glShadeModel(GL_SMOOTH);

  glClearColor( 0.8, 0.8, 1.0, 1.0 );

  //Quaternion trackball variables, you can ignore
  GLState::scaling  = 0;
  GLState::moving   = 0;
  GLState::panning  = 0;
  GLState::beginx   = 0;
  GLState::beginy   = 0;

  TrackBall::matident(GLState::curmat);
  TrackBall::trackball(GLState::curquat , 0.0f, 0.0f, 0.0f, 0.0f);
  TrackBall::trackball(GLState::lastquat, 0.0f, 0.0f, 0.0f, 0.0f);
  TrackBall::add_quats(GLState::lastquat, GLState::curquat, GLState::curquat);
  TrackBall::build_rotmatrix(GLState::curmat, GLState::curquat);

  GLState::scalefactor = 1.0;
  GLState::render_line = false;

}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void drawObject(Object * object, GLuint vao, GLuint buffer){

  color4 material_ambient(object->shadingValues.color.x*object->shadingValues.Ka,
                          object->shadingValues.color.y*object->shadingValues.Ka,
                          object->shadingValues.color.z*object->shadingValues.Ka, 1.0 );
  color4 material_diffuse(object->shadingValues.color.x,
                          object->shadingValues.color.y,
                          object->shadingValues.color.z, 1.0 );
  color4 material_specular(object->shadingValues.Ks,
                           object->shadingValues.Ks,
                           object->shadingValues.Ks, 1.0 );
  float  material_shininess = object->shadingValues.Kn;

  color4 ambient_product  = GLState::light_ambient * material_ambient;
  color4 diffuse_product  = GLState::light_diffuse * material_diffuse;
  color4 specular_product = GLState::light_specular * material_specular;

  glUniform4fv( glGetUniformLocation(GLState::program, "AmbientProduct"), 1, ambient_product );
  glUniform4fv( glGetUniformLocation(GLState::program, "DiffuseProduct"), 1, diffuse_product );
  glUniform4fv( glGetUniformLocation(GLState::program, "SpecularProduct"), 1, specular_product );
  glUniform4fv( glGetUniformLocation(GLState::program, "LightPosition"), 1, lightPosition );
  glUniform1f(  glGetUniformLocation(GLState::program, "Shininess"), material_shininess );

  glBindVertexArray(vao);
  glBindBuffer( GL_ARRAY_BUFFER, buffer );
  glVertexAttribPointer( GLState::vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
  glVertexAttribPointer( GLState::vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(object->mesh.vertices.size()*sizeof(vec4)) );

  mat4 objectModelView = GLState::sceneModelView*object->getModelView();


  glUniformMatrix4fv( GLState::ModelViewLight, 1, GL_TRUE, GLState::sceneModelView);
  glUniformMatrix3fv( GLState::NormalMatrix, 1, GL_TRUE, Normal(objectModelView));
  glUniformMatrix4fv( GLState::ModelView, 1, GL_TRUE, objectModelView);

  glDrawArrays( GL_TRIANGLES, 0, object->mesh.vertices.size() );

}


int main(void){

  GLFWwindow* window;

  glfwSetErrorCallback(error_callback);

  if (!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  glfwWindowHint(GLFW_SAMPLES, 4);

  window = glfwCreateWindow(768, 768, "Raytracer", NULL, NULL);
  if (!window){
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwSetKeyCallback(window, keyCallback);
  glfwSetMouseButtonCallback(window, mouseClick);
  glfwSetCursorPosCallback(window, mouseMove);


  glfwMakeContextCurrent(window);
  gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
  glfwSwapInterval(1);

  switch(scene){
    case _SPHERE:
      initUnitSphere();
      break;
    case _SQUARE:
      initUnitSquare();
      break;
    case _BOX:
      initCornellBox();
      break;
    case _SCENE:
      initScene();
      break;
  }

  initGL();

  while (!glfwWindowShouldClose(window)){

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    GLState::window_height = height;
    GLState::window_width  = width;

    glViewport(0, 0, width, height);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mat4 track_ball =  mat4(GLState::curmat[0][0], GLState::curmat[1][0],
                            GLState::curmat[2][0], GLState::curmat[3][0],
                            GLState::curmat[0][1], GLState::curmat[1][1],
                            GLState::curmat[2][1], GLState::curmat[3][1],
                            GLState::curmat[0][2], GLState::curmat[1][2],
                            GLState::curmat[2][2], GLState::curmat[3][2],
                            GLState::curmat[0][3], GLState::curmat[1][3],
                            GLState::curmat[2][3], GLState::curmat[3][3]);

    GLState::sceneModelView  =  Translate(-cameraPosition) *   //Move Camera Back
    Translate(GLState::ortho_x, GLState::ortho_y, 0.0) *
    track_ball *                   //Rotate Camera
    Scale(GLState::scalefactor,
          GLState::scalefactor,
          GLState::scalefactor);   //User Scale

    GLfloat aspect = GLfloat(width)/height;

    switch(scene){
      case _SPHERE:
      case _SQUARE:
        GLState::projection = Perspective( 45.0, aspect, 0.01, 100.0 );
        break;
      case _BOX:
        GLState::projection = Perspective( 45.0, aspect, 4.5, 100.0 );
        break;
    }

    glUniformMatrix4fv( GLState::Projection, 1, GL_TRUE, GLState::projection);

    for(unsigned int i=0; i < sceneObjects.size(); i++){
      drawObject(sceneObjects[i], GLState::objectVao[i], GLState::objectBuffer[i]);
    }

    glfwSwapBuffers(window);
    glfwPollEvents();

  }

  glfwDestroyWindow(window);

  glfwTerminate();
  exit(EXIT_SUCCESS);
}
