//////////////////////////////////////////////////////////////////////////////
//
//  --- Object.h ---
//  Created by Brian Summa
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include "common.h"

#define EPSILON  1e-3

class Object{
public:

    std::string name;

    friend class Sphere;
    friend class Square;

    typedef struct{
        vec4 color;
        float emission = 0;
        float roughness = 0.9;
        float transmission = 0.8;
        float refraction = 0;
        float ior = 1.333;
        float metallic = 0;
        float Kd;
        float Ks;
        float Kn;
        float Kt;
        float Ka;
        float Kr;
    } ShadingValues;

    typedef struct{
        double t;
        double u;
        double v;
        vec4 P;
        vec4 N;
        int ID_;
        std::string name;
        Object *o = nullptr;
    } IntersectionValues;


    Object(std::string name): name(name)  {};
    ~Object() {};

    Mesh mesh;
    ShadingValues shadingValues;

private:
    mat4 C;
    mat4 INVC;
    mat4 INVCStar;
    mat4 TRANINVC;

public:

    bool rayTriangleIntersect(
        const vec3 p0, 
        const vec3 V, 
        const vec3 a, 
        const vec3 b, 
        const vec3 c,  
        double &t, 
        double &u, 
        double &v);

    
    bool meshIntersect(
        const vec3 p0,
        const vec3 V,
        double &t,
        double &u, 
        double &v,
        vec4& N);

    void setShadingValues(ShadingValues _shadingValues){shadingValues = _shadingValues;}

    void setModelView(mat4 modelview){
        C = modelview;
        INVC = invert(modelview);
        mat4 CStar = modelview;
        CStar[0][3] = 0;
        CStar[1][3] = 0;
        CStar[2][3] = 0;
        INVCStar = invert(CStar);
        TRANINVC = transpose(invert(modelview));
    }

    mat4 getModelView(){ return C; }

    virtual IntersectionValues intersect(vec3 p, vec3 v)=0;


};

class Sphere : public Object{
public:
    
    Sphere(std::string name, vec3 center= vec3(0., 0., 0.), double radius=1.) : Object(name), center(center), radius(radius) { 
        mesh.makeSubdivisionSphere(1, center, radius); 
    };
    
    virtual IntersectionValues intersect(vec3 p, vec3 v);
    
private:
    double raySphereIntersection(
        vec3 p0, 
        vec3 V);
    vec3 center;
    double radius;
};


class Square : public Object{
public:

    Square(std::string name, mat4 transform = mat4()) : Object(name) {

        mesh.vertices.resize(6);
        mesh.uvs.resize(6);
        mesh.normals.resize(6);

        mesh.vertices[1]=transform*vec4(1.0, 1.0, 0.0, 1.0);
        mesh.uvs[1] = vec2(1.0,1.0);
        mesh.vertices[0]=transform*vec4(-1.0, -1.0, 0.0, 1.0);
        mesh.uvs[0] = vec2(0.0,0.0);
        mesh.vertices[2]=transform*vec4(1.0, -1.0, 0.0, 1.0);
        mesh.uvs[2] = vec2(1.0,0.0);

        mesh.vertices[3]=transform*vec4(1.0, 1.0, 0.0, 1.0);
        mesh.uvs[3] = vec2(1.0,1.0);
        mesh.vertices[4]=transform*vec4(-1.0, -1.0, 0.0, 1.0);
        mesh.uvs[4] = vec2(0.0,0.0);
        mesh.vertices[5]=transform*vec4(-1.0, 1.0, 0.0, 1.0);
        mesh.uvs[5] = vec2(0.0,1.0);

        point = mesh.vertices[0];
        TRANINVC = transpose(invert(transform));
        vec4 N (0, 0, 1.0, 0.);
        N = TRANINVC*N;
        normal = vec3(N.x, N.y, N.z);
        for( unsigned i = 0 ; i < 6 ; i++){
            mesh.normals[i]= normal;
        }

    };

    virtual IntersectionValues intersect(vec3 p, vec3 v);

private:
    double raySquareIntersection(vec4 p0, vec4 V);
    vec4 point;
    vec3 normal;
};

class Box : public Object {
    private:
    Square *faces[6];

    public:
    Box(std::string name, mat4 transform = mat4()) : Object(name) {
        faces[0] = new Square("_Front",transform *  RotateX(180)*    Translate(0, 0, 1));
        faces[1] = new Square("_Back", transform *                   Translate(0, 0, 1));
        faces[2] = new Square("_Left", transform *  RotateX(90)*     Translate(0, 0, 1));
        faces[3] = new Square("_Right",transform *  RotateX(-90)*    Translate(0, 0, 1));
        faces[4] = new Square("_Left", transform *  RotateY(90)*     Translate(0, 0, 1));
        faces[5] = new Square("_Right",transform *  RotateY(-90)*    Translate(0, 0, 1));
    }

    IntersectionValues intersect(vec3 p, vec3 v) {
        IntersectionValues iv;
        iv.t = INFINITY;

        for (int i = 0; i < 6; i++) {
            IntersectionValues interVal = faces[i]->intersect(p, v);
            if (interVal.t < iv.t) {
                iv = interVal;
            }
        }

        return iv;
    }

};

class MeshObj : public Object {
public:     
    MeshObj (std::string name, char* path) : Object(name) {
        mesh.loadOBJ(path);
    }

    virtual IntersectionValues intersect(vec3 p, vec3 v);
};
#endif /* defined(__OBJECT_H__) */
