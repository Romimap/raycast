//////////////////////////////////////////////////////////////////////////////
//
//  --- Object.cpp ---
//  Created by Brian Summa
//
//////////////////////////////////////////////////////////////////////////////


#include "common.h"

bool Object::meshIntersect(
  const vec3 p0,
  const vec3 V,
  double &t,
  double &u, 
  double &v,
  vec4& N) {
    t = INFINITY;
    u = 0;
    v = 0;
    for(int i = 0; i < mesh.vertices.size(); i+=3) {
      vec4 a = mesh.vertices[i];
      vec4 b = mesh.vertices[i + 1];
      vec4 c = mesh.vertices[i + 2];

      vec3 A = vec3(a.x, a.y, a.z);
      vec3 B = vec3(b.x, b.y, b.z);
      vec3 C = vec3(c.x, c.y, c.z);

      double t1, u1, v1;

      if (rayTriangleIntersect(p0, V, A, B, C, t1, u1, v1)) {
        if (t1 < t) {
          t = t1;
          u = u1;
          v = v1;
          N = cross(b-a, b-c); 
        }
      }
    }
    return t != INFINITY;
  }

/* Möller–Trumbore */
bool Object::rayTriangleIntersect(
  const vec3 p0, 
  const vec3 V, 
  const vec3 a, 
  const vec3 b, 
  const vec3 c,  
  double &t, 
  double &u, 
  double &v) {
    const float epsilon = 0.0000001;
    vec3 ab, ac, h, s, q;
    float e,f;
    ab = b - a;
    ac = c - a;
    h = cross(V, ac);
    e = dot(ab, h);
    if (e > -epsilon && e < epsilon)
        return false;    // parallel

    f = 1.0/e;
    s = p0 - a;
    u = f * (dot(s, h));
    if (u < 0.0 || u > 1.0)
        return false;
    q = cross(s, ab);
    v = f * dot(V, q);
    if (v < 0.0 || u + v > 1.0)
        return false;

    t = f * dot(ac, q);
    return t > epsilon;
} 


/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
Object::IntersectionValues Square::intersect(vec3 p, vec3 v){
  IntersectionValues result;

  meshIntersect(p, v, result.t, result.u, result.v, result.N);
  result.o = this;

  return result;
}

// /* -------------------------------------------------------------------------- */
// /* -------------------------------------------------------------------------- */
// Object::IntersectionValues Sphere::intersect(vec3 p, vec3 v){
//   IntersectionValues result;

//   // meshIntersect(p, v, result.t, result.u, result.v, result.N);

//   raySphereIntersection(p, v, result.t, result.u, result.v, result.N);
  
//   return result;
// }


/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
double Square::raySquareIntersection(vec4 p0, vec4 V){
  double t   = std::numeric_limits< double >::infinity();
  //TODO: Ray-square intersection;
  return t;
}



/* -------------------------------------------------------------------------- */
/* ------ Ray = p0 + t*V  sphere at origin center and radius radius    : Find t ------- */
double Sphere::raySphereIntersection(vec3 p0, vec3 V){
  double t = INFINITY;
  
  //t2 d.d + t 2(d.(o-c)) + ||o-c||² – r² = 0
  //a = d.d
  //b = 2(d.(o-c))
  //c = ||o-c||² –r²
  double a = dot(V, V);
  double b = 2 * dot(V, p0 - center);
  double l = length(p0 - center);
  double c = l*l - radius*radius;

  double delta = (b*b) - (4*a*c);

  if (delta < 0) //Negative delta, no intersection
    return t;

  double rdelta = sqrt(delta);
  double x1 = (-b - rdelta) / (2*a);
  double x2 = (-b + rdelta) / (2*a);

  if (x1 < 0 && x2 < 0) {
    return t;
  }

  if (x1 < 0) {
    return x2;
  }

  if (x2 < 0) {
    return x1;
  }

  return std::min(x1, x2);
}

Object::IntersectionValues Sphere::intersect(vec3 p0, vec3 V){
  IntersectionValues result;
  
  // meshIntersect(p0, V, result.t, result.u, result.v, result.N);
  // result.o = this;


  double t1 = raySphereIntersection(p0, V);
  if (t1 < result.t) {
    result.t = t1;
    vec3 p = p0 + V * t1;
    vec3 n = (p - center);
    n = normalize(n);
    result.N = vec4(n, 1);
    result.o = this;
  }

  return result;
}

Object::IntersectionValues MeshObj::intersect(vec3 p0, vec3 V){
  IntersectionValues result;
  
  meshIntersect(p0, V, result.t, result.u, result.v, result.N);
  result.o = this;

  return result;
}
