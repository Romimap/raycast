#include <vector>
#include <stdlib.h>
#include "Object.h"
#include "vec.h"
 

class Octree {
public:
    struct ObjectChunk {
        Object *o;
        std::vector<vec3> * triangles = new std::vector<vec3>();
    };

    std::vector<ObjectChunk*> * chunks = new std::vector<ObjectChunk*>();
    Octree *parent;
    /*
          6-----7
         /     /|
        2-----3 |
        | 4   | 5  y  z
        |     |/   | /
        0-----1    .---x
    */
    Octree *children[8] = {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr};
    bool leaf = true;
    vec3 bounds[2];
    int level = 0;
    int totalTris = 0;
    //Right Left Up Down Front Back
    Octree *neighbor[6] = {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr}; 

    //p0 plane origin, n normal, l0 origin, l direction 
    bool intersectPlane(const vec3 &n, const vec3 &p0, const vec3 &l0, const vec3 &l, float &t) { 
        float denom = dot(n, l); 
        if (denom > 1e-6) { 
            vec3 p0l0 = p0 - l0; 
            t = dot(p0l0, n) / denom; 
            return (t >= 0); 
        } 
    
        return false; 
    } 


    Object::IntersectionValues intersect (vec3 p0, vec3 E) {
        //Current octree have children, find the one that have to handle the ray
        if (children[0] != nullptr) {
            Octree *next;

            /*
                  6-----7
                 /     /|
                2-----3 |
                | 4   | 5  y  z
                |     |/   | /
                0-----1    .---x
             */
            int mask = 0;
            vec3 mid;
            mid.x = (bounds[0].x + bounds[1].x)/2.;
            mid.y = (bounds[0].y + bounds[1].y)/2.;
            mid.z = (bounds[0].z + bounds[1].z)/2.;
            if (p0.x > mid.x) mask += 1; //right
            if (p0.z > mid.z) mask += 2; //front
            if (p0.y > mid.y) mask += 4; //top

            next = children[mask];

            return next->intersect(p0, E);
        }


        //Intersection w/ all triangles in the octree
        double t = INFINITY;
        double u = 0;
        double v = 0;
        vec3 N;
        Object *O;
        
        for (ObjectChunk *oc : *chunks) {
            for(int i = 0; i < oc->triangles->size(); i+=3) {
                vec4 a = oc->triangles->at(i);
                vec4 b = oc->triangles->at(i + 1);
                vec4 c = oc->triangles->at(i + 2);

                vec3 A = vec3(a.x, a.y, a.z);
                vec3 B = vec3(b.x, b.y, b.z);
                vec3 C = vec3(c.x, c.y, c.z);

                double t1, u1, v1;

                if (rayTriangleIntersect(p0, E, A, B, C, t1, u1, v1)) {
                    if (t1 < t) {
                        t = t1;
                        u = u1;
                        v = v1;
                        N = cross(b-a, b-c); 
                        O = oc->o;
                    }
                }
            }
        }

        //Intersection found, END
        if (t < INFINITY) { 
            Object::IntersectionValues intersectionValues;
            intersectionValues.t = t;
            intersectionValues.u = u;
            intersectionValues.v = v;
            intersectionValues.N = N;
            intersectionValues.o = O;

            return intersectionValues;
        }

        //No intersection, propagate to the next octree node
        Octree *next;
        vec3 newp0;
        vec3 newE;

        float minTBox = INFINITY;
        float tBox = 0;
        int direction = 0;
        if (E.x > 0) { //Right
            if (intersectPlane(vec3(1, 0, 0), bounds[1], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 0;
                }
            }
        } else { //Left
            if (intersectPlane(vec3(-1, 0, 0), bounds[0], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 1;
                }
            }
        }
        if (E.y > 0) { //Up
            if (intersectPlane(vec3(0, 1, 0), bounds[1], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 2;
                }
            }
        } else { //Down
            if (intersectPlane(vec3(0, -1, 0), bounds[0], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 3;
                }
            }
        }
        if (E.z > 0) { //Front
            if (intersectPlane(vec3(0, 0, 1), bounds[1], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 4;
                }
            }
        } else { //Back
            if (intersectPlane(vec3(0, 0, -1), bounds[0], p0, E, tBox)) {
                if (tBox < minTBox) {
                    minTBox = tBox;
                    direction = 5;
                }
            }
        }

        Octree *current = this;
        next = current->neighbor[direction];
        while (next == nullptr && current->parent != nullptr) {
            current = current->parent;
            next = current->neighbor[direction];
        }

        //No next = We exit the octree, END
        if (next == nullptr) { 
            Object::IntersectionValues intersectionValues;
            intersectionValues.t = INFINITY;
            return intersectionValues;
        }

        //Ask the next octree to handle the collisions
        return next->intersect(newp0, newE);
    }

    /* Möller–Trumbore */
    bool rayTriangleIntersect(
    const vec3 p0, 
    const vec3 V, 
    const vec3 a, 
    const vec3 b, 
    const vec3 c,  
    double &t, 
    double &u, 
    double &v) {
        const float epsilon = 0.0000001;
        vec3 ab, ac, h, s, q;
        float e,f;
        ab = b - a;
        ac = c - a;
        h = cross(V, ac);
        e = dot(ab, h);
        if (e > -epsilon && e < epsilon)
            return false;    // parallel

        f = 1.0/e;
        s = p0 - a;
        u = f * (dot(s, h));
        if (u < 0.0 || u > 1.0)
            return false;
        q = cross(s, ab);
        v = f * dot(V, q);
        if (v < 0.0 || u + v > 1.0)
            return false;

        t = f * dot(ac, q);
        return t > epsilon;
    } 

    //TODO: detect triangles that intersects the box, not only the ones that have a vertex in the box
    //      good enough for testing
    void bake() {
        printf("bake start\n");
        for(ObjectChunk *c : *(parent->chunks)) {
            printf("  chunk\n");

            ObjectChunk *objectChunk = new ObjectChunk();
            objectChunk->o = c->o;
            for (int i = 0; i < c->triangles->size(); i+=3) {
                vec3 &A = c->triangles->at(i);
                vec3 &B = c->triangles->at(i + 1);
                vec3 &C = c->triangles->at(i + 2); 
                if (
                   (A.x < bounds[1].x && A.x > bounds[0].x
                &&  A.y < bounds[1].y && A.y > bounds[0].y
                &&  A.z < bounds[1].z && A.z > bounds[0].z)
                ||
                   (B.x < bounds[1].x && B.x > bounds[0].x
                &&  B.y < bounds[1].y && B.y > bounds[0].y
                &&  B.z < bounds[1].z && B.z > bounds[0].z)
                ||
                   (C.x < bounds[1].x && C.x > bounds[0].x
                &&  C.y < bounds[1].y && C.y > bounds[0].y
                &&  C.z < bounds[1].z && C.z > bounds[0].z)) {
                    objectChunk->triangles->push_back(A);
                    objectChunk->triangles->push_back(B);
                    objectChunk->triangles->push_back(C);
                    totalTris++;
                }
            }
            if (objectChunk->triangles->size() > 0)
                chunks->push_back(objectChunk);
            else
                free(objectChunk);
        }
        subdivide();
        printf("bake end\n");
    }

    void subdivide () {
        printf("subdivide start\n");
        if (level > 8 || totalTris == 0) return;
        if (!leaf) return;
        leaf = false;

        float d = abs((bounds[1].x - bounds[0].x)) / 2.;

        children[0] = new Octree();
        children[0]->parent = this;
        children[0]->level = level + 1;
        children[0]->bounds[0] = bounds[0] + vec3(0, 0, 0);
        children[0]->bounds[1] = children[0]->bounds[0] + vec3(d, d, d);
        children[0]->bake();

        children[1] = new Octree();
        children[1]->parent = this;
        children[1]->level = level + 1;
        children[1]->bounds[0] = bounds[0] + vec3(d, 0, 0);
        children[1]->bounds[1] = children[1]->bounds[0] + vec3(d, d, d);
        children[1]->bake();

        children[2] = new Octree();
        children[2]->parent = this;
        children[2]->level = level + 1;
        children[2]->bounds[0] = bounds[0] + vec3(0, d, 0);
        children[2]->bounds[1] = children[2]->bounds[0] + vec3(d, d, d);
        children[2]->bake();

        children[3] = new Octree();
        children[3]->parent = this;
        children[3]->level = level + 1;
        children[3]->bounds[0] = bounds[0] + vec3(d, d, 0);
        children[3]->bounds[1] = children[3]->bounds[0] + vec3(d, d, d);
        children[3]->bake();

        children[4] = new Octree();
        children[4]->parent = this;
        children[4]->level = level + 1;
        children[4]->bounds[0] = bounds[0] + vec3(0, 0, d);
        children[4]->bounds[1] = children[4]->bounds[0] + vec3(d, d, d);
        children[4]->bake();

        children[5] = new Octree();
        children[5]->parent = this;
        children[5]->level = level + 1;
        children[5]->bounds[0] = bounds[0] + vec3(d, 0, d);
        children[5]->bounds[1] = children[5]->bounds[0] + vec3(d, d, d);
        children[5]->bake();

        children[6] = new Octree();
        children[6]->parent = this;
        children[6]->level = level + 1;
        children[6]->bounds[0] = bounds[0] + vec3(0, d, d);
        children[6]->bounds[1] = children[6]->bounds[0] + vec3(d, d, d);
        children[6]->bake();

        children[7] = new Octree();
        children[7]->parent = this;
        children[7]->level = level + 1;
        children[7]->bounds[0] = bounds[0] + vec3(d, d, d);
        children[7]->bounds[1] = children[7]->bounds[0] + vec3(d, d, d);
        children[7]->bake();

    /*
          6-----7
         /     /|
        2-----3 |
        | 4   | 5  y  z
        |     |/   | /
        0-----1    .---x
    */
    //   0       1       2       3       4       5
    //Right     Left    Up      Down    Front   Back

        children[7]->neighbor[1] = children[6];
        children[7]->neighbor[3] = children[5];
        children[7]->neighbor[5] = children[3];

        children[6]->neighbor[0] = children[7];
        children[6]->neighbor[3] = children[4];
        children[6]->neighbor[5] = children[2];

        children[5]->neighbor[1] = children[4];
        children[5]->neighbor[2] = children[7];
        children[5]->neighbor[5] = children[1];

        children[4]->neighbor[0] = children[5];
        children[4]->neighbor[2] = children[6];
        children[4]->neighbor[5] = children[0];

        children[3]->neighbor[1] = children[2];
        children[3]->neighbor[3] = children[1];
        children[3]->neighbor[4] = children[7];

        children[2]->neighbor[0] = children[3];
        children[2]->neighbor[3] = children[0];
        children[2]->neighbor[4] = children[6];

        children[1]->neighbor[1] = children[0];
        children[1]->neighbor[2] = children[3];
        children[1]->neighbor[4] = children[5];

        children[0]->neighbor[0] = children[1];
        children[0]->neighbor[2] = children[2];
        children[0]->neighbor[4] = children[4];
    }
};